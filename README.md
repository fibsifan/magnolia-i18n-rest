# Magnolia I18n App

An app that offers an editor for translations.

Through a Rest-API headless frontends or other applications can add localization keys with initial values in the default
language that need to be translated.
