package de.jball.magnolia.i18n.service;

import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Optional;

public interface L10nService {
    /**
     * Get l10n for the given path and depth, returns a json map.
     *
     * @param path  Path from where to load. Relative to /.
     * @param depth max depth for the json map (relative to root, not to the path).
     * @return An optional, which is empty if the path doesn't exist or with a map of translations.
     */
    Optional<Map<String, Object>> getI18n(String path, int depth);

    /**
     * Translations from the given map are added under the path but only if they're not yet existing.
     *
     * @param path         Path where to add the translations, relative to /
     * @param translations Json map of the translations to add.
     * @return If an error occured this will a corresponding status.
     */
    Optional<Response.Status> addAndPublishMissingTranslations(String path, Map<String, Object> translations);
}
